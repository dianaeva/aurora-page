// For clicking
const register = document.getElementById('register');
const basket = document.getElementById('basket');
const circle = document.getElementById('circle');
const intro = document.getElementById('intro');

// For changing to active mode
const headerAbove = document.getElementById('header-above');
const introWrapper = document.getElementById('intro-wrapper');
const mainHeaderWrapper = document.getElementById('main-header-wrapper');
const searchPanel = document.getElementById('search-panel');
const searchInput = document.getElementById('search-input');
const signIn = document.getElementById('sign-in');
const navigation = document.getElementById('navigation');

// Array of tags for changing to active mode
const tagsToActive = [headerAbove, introWrapper, mainHeaderWrapper, searchPanel, searchInput,signIn,
navigation];

// Images for changing
const logoAurora = document.getElementById('logoAurora');
const iconFill = document.getElementById('iconFill');
const iconBasket = document.getElementById('iconBasket');

let userName = '';

// Function that counts an amount of clicks on the Basket
register.onclick = () => {
    userName = prompt('What is your name?', 'Angela');
    register.innerHTML = `Hi, ${userName}!`;
};

// Onclick events
// Function that counts an amount of clicks on the Basket
let amountOfPurchases = 0;
basket.onclick = () => {
    amountOfPurchases++;
    circle.style.display = 'flex';
    circle.innerHTML = amountOfPurchases;
}

intro.onclick = transformToActiveMode;

logoAurora.onclick = transformToClassicMode;

// Function for transformation to active mode
function transformTagsToActiveMode(arr){
    for(let item of arr) {
        item.classList.add('active');
    }
}

function transformTagsToClassicMode(arr){
    for(let item of arr) {
        item.classList.remove('active');
    }
}

function transformImagesToActiveMode() {
    logoAurora.src="images/icons/AuroraBlack.svg";
    iconFill.src="images/icons/Fill 1Black.png";
    iconBasket.src="images/icons/basketBlack.svg";
}

function transformImagesToClassicMode() {
    logoAurora.src="images/icons/Aurora.svg";
    iconFill.src="images/icons/Fill 1.png";
    iconBasket.src="images/icons/basketWhite.svg";
}

// Function requires an array of tags, which will change
function transformToActiveMode(){
    transformTagsToActiveMode(tagsToActive);
    transformImagesToActiveMode();
}

function transformToClassicMode(){
    transformTagsToClassicMode(tagsToActive);
    transformImagesToClassicMode();
}

//Mobile Framework
// Function for accordion

const acc = document.getElementsByClassName("accordion-mobile");

for(let item of acc){
    item.addEventListener("click", function() {
        this.classList.toggle("opened-mobile");
        let panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
}